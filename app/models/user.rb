require 'bcrypt'

class User < ActiveRecord::Base
  attr_accessible :email, :password
  attr_reader :password

  validates :email, presence: true, uniqueness: true
  validates :password, presence: {message: "Password can't be bla`nk" }
  validates :password, length: {minimum: 3, too_short: "must be at least 3 characters long"}
  validates :password_digest, presence: true
  validates :session_token, presence: true
  validates :activation_token, presence: true

  before_validation :reset_session_token!
  
  before_validation :create_activation_token, on: :create

  has_many(
  :notes,
  class_name: "Note",
  foreign_key: :user_id,
  primary_key: :id
  )


  def self.find_by_credentials(email, secret)
    user = User.find_by_email(email)
    user.is_password?(secret) ? user : nil
  end

  def reset_session_token!
    self.session_token = User.generate_session_token
  end

  def self.generate_session_token
    SecureRandom::urlsafe_base64(16)
  end
  
  def create_activation_token
    write_attribute(:activation_token, SecureRandom::urlsafe_base64(16))
  end

  def password=(secret)
    @password = secret
    write_attribute(:password_digest, BCrypt::Password.create(secret))
  end

  def is_password?(secret)
    BCrypt::Password.new(self.password_digest).is_password?(secret)
  end

end
