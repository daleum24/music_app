class Album < ActiveRecord::Base
  attr_accessible :album_title, :album_type, :band_id

  validates :album_title, presence: true, uniqueness: true
  validates :album_type, presence: true, inclusion: { in: %w(live studio), message:"%{value} is not a valid album type" }
  validates :band_id, presence: true

  belongs_to(
  :band,
  class_name: "Band",
  foreign_key: :band_id,
  primary_key: :id
  )

  has_many(
  :tracks,
  dependent: :destroy,
  class_name: "Track",
  foreign_key: :album_id,
  primary_key: :id
  )

end
