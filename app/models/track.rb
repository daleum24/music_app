class Track < ActiveRecord::Base
  attr_accessible :track_title, :track_type, :lyrics, :album_id

  validates :track_title, presence: true, uniqueness: true
  validates :track_type, presence: true, inclusion: { in: %w(bonus regular), message:"%{value} is not a valid track type" }
  validates :lyrics, presence: true
  validates :album_id, presence: true

  belongs_to(
  :album,
  class_name: "Album",
  foreign_key: :album_id,
  primary_key: :id
  )

  has_many(
  :notes,
  class_name: "Note",
  foreign_key: :track_id,
  primary_key: :id
  )


end
