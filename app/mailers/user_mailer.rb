class UserMailer < ActionMailer::Base
  default from: "admin@musicapp.com"

  def activation_email(user)
    @user = user
    @token = user.activation_token
    @url = activate_users_url + "?activation_token=#{@token}" 
    mail(to: user.email, subject: 'Activate Your Account')
  end

end
