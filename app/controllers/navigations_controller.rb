class NavigationsController < ApplicationController

  skip_before_filter :require_login, :only => :index

  def index
    render :index
  end

end
