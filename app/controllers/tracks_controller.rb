class TracksController < ApplicationController

  def new
    @album = Album.find(params[:album_id])
    @track = Track.new
    render :new
  end

  def create
    new_track = Track.new(params[:track])
    if new_track.save
      redirect_to track_url
    else
      render json: new_track.errors.full_messages
    end
  end

  def update
    updated_track = Track.find_by_id(params[:id])
    if updated_track.update_attributes(params[:track])
      redirect_to updated_track
    else
      render json: updated_track.errors.full_messages
    end
  end

  def edit
    @track = Track.find(params[:id])
    render :edit
  end

  def show
    @track = Track.find(params[:id])
    render :show
  end

  def destroy
    @current_album = Track.find(params[:id]).album
    Track.destroy(params[:id])
    redirect_to @current_album
  end

end
