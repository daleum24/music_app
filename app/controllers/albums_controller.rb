class AlbumsController < ApplicationController

  def new
    @band = Band.find(params[:band_id])
    @album = Album.new
    render :new
  end

  def create
    new_album = Album.new(params[:album])
    if new_album.save
      redirect_to new_album
    else
      render json: new_album.errors.full_messages
    end
  end

  def update
    current_album = Album.find(params[:id])
    if current_album.update_attributes(params[:band])
      redirect_to current_album
    else
      render json: current_album.errors.full_messages
    end
  end

  def edit
    @album = Album.find(params[:id])
    @band = @album.band
    render :edit
  end
  #
  def show
    @album = Album.find(params[:id])
    render :show
  end

  def destroy
    @current_band = Album.find(params[:id]).band
    Album.destroy(params[:id])
    redirect_to @current_band
  end

end
