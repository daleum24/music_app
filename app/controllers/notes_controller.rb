class NotesController < ApplicationController

  def create
    new_note = Note.new(params[:note])
    if new_note.save
      redirect_to track_url(params[:note][:track_id])
    else
      new_note.errors.full_messages
    end
  end

  def destroy
    current_note = Note.find(params[:id])
    track_id = current_note.track_id
    if Note.destroy(params[:id])
      redirect_to track_url(track_id)
    else
      current_note.errors.full_messages
    end
  end

end
