class BandsController < ApplicationController

  def index
    @all_bands = Band.all
    render :index
  end

  def new
    @band = Band.new
    render :new
  end

  def create
    new_band = Band.new(params[:band])
    if new_band.save
      redirect_to bands_url
    else
      render json: new_band.errors.full_messages
    end
  end

  def update
    updated_band = Band.find_by_id(params[:id])
    if updated_band.update_attributes(params[:band])
      redirect_to updated_band
    else
      render json: updated_band.errors.full_messages
    end
  end

  def edit
    @band = Band.find(params[:id])
    render :edit
  end

  def show
    @band = Band.find(params[:id])
    render :show
  end

  def destroy
    Band.destroy(params[:id])
    redirect_to bands_url
  end

end
