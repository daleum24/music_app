class UsersController < ApplicationController
  skip_before_filter :require_login, only: [:new, :create, :activate]

  def new
    render :new
  end

  def create
    new_user = User.new(params[:user])
    if new_user.save
      UserMailer.activation_email(new_user).deliver!
      redirect_to new_session_url
    else
      render json: new_user.errors.full_messages
    end
  end

  def show
    render :show
  end

  def activate
    user = User.find_by_activation_token(params[:activation_token])
    user.update_attribute(:activated, true)
    redirect_to new_session_url
  end

end
