class SessionsController < ApplicationController
  skip_before_filter :require_login, only: [:new, :create]

  def new
    render :new
  end

  def create
    login_user!
  end

  def destroy
    logout!
  end

end
