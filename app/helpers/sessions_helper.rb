module SessionsHelper

  def current_user=(user)
    @current_user = user
    session[:session_token] = user.session_token
  end

  def current_user
    return nil if session[:session_token].nil?
    @current_user ||= User.find_by_session_token(session[:session_token])
  end

  def logged_in?
    current_user ? true : false
  end

  def login_user!
    login_email    = params[:user][:email]
    login_password = params[:user][:password]

    new_user = User.find_by_credentials(login_email, login_password)

    if new_user && new_user.is_password?(login_password) 
      if new_user.activated == false
        render text: "Please activate your account"
      else
        self.current_user = new_user
        redirect_to bands_url
      end
    else
      render text: "Invalid email/password combination"
    end
  end

  def logout!
    self.current_user.reset_session_token!
    session[:session_token] = nil
    redirect_to :root
  end

end
