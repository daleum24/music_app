class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.string  :track_title, null: false
      t.string  :track_type, null: false
      t.text    :lyrics, null: false
      t.integer :album_id, null: false

      t.timestamps
    end
    add_index :tracks, :track_title, unique: true
    add_index :tracks, :album_id
  end
end
