# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create([
  {email: "dale@appacademy.io", password: "meow"},
])


Band.create([
  {band_name: "Bingo Players"}
])


Album.create([
  {album_title: "Bingo Players Album 1", album_type: "studio", band_id: 1},
  {album_title: "Bingo Players Album 2", album_type: "studio", band_id: 1},
])

Track.create([
  {track_title: "Cry", track_type: "regular", album_id: 1, lyrics: "meow"},
  {track_title: "Rattle", track_type: "regular", album_id: 1, lyrics: "meow"},
  {track_title: "Don't Blame the Party", track_type: "regular", album_id: 2, lyrics: "meow"}
])

Note.create([
  {title: "title 1", body: "body 1", user_id: 1, track_id: 1},
  {title: "title 2", body: "body 2", user_id: 1, track_id: 1},
  {title: "title 3", body: "body 3", user_id: 1, track_id: 1}
])
















